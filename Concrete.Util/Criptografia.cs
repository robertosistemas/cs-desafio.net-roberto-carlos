﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Concrete.Util
{
    public class Criptografia
    {

        /// <summary>
        /// Gera um hash a partir de um texto e um salt fornecidos
        /// </summary>
        /// <param name="input">texto para gerar hash</param>
        /// <param name="salt">Salt para utilizar junto com o texto para gerar o hash</param>
        /// <returns></returns>
        public static string GerarHashMd5(string input, string salt)
        {
            MD5 md5Hash = MD5.Create();
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(string.Format("{0}{1}", input, salt)));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }

        /// <summary>
        /// Cria um Salt Aleatório para ser usado na criptografia de senhas e tokens
        /// </summary>
        /// <returns></returns>
        public static string GerarSalt()
        {
            var data = new byte[0x10];
            using (var cryptoServiceProvider = new RNGCryptoServiceProvider())
            {
                cryptoServiceProvider.GetBytes(data);
                return Convert.ToBase64String(data);
            }
        }

        /// <summary>
        ///  Criptografa uma senha usando o Salt fornecido
        /// </summary>
        /// <param name="input">texto para gerar hash</param>
        /// <param name="salt">Salt para utilizar junto com o texto para gerar o hash</param>
        /// <returns></returns>
        public static string Criptografa(string input, string salt)
        {
            using (var sha256 = SHA256.Create())
            {
                var saltedPassword = string.Format("{0}{1}", salt, input);
                var saltedPasswordAsBytes = Encoding.UTF8.GetBytes(saltedPassword);
                return Convert.ToBase64String(sha256.ComputeHash(saltedPasswordAsBytes));
            }
        }

    }
}
