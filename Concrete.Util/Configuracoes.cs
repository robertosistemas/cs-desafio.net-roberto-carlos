﻿using System;
using System.Configuration;
using System.IO;

namespace Concrete.Util
{
    public enum TipoBanco
    {
        Sqlite = 1,
        SqlServer = 2,
        Firebird = 3,
        Oracle = 4
    }

    public class Configuracoes
    {
        /// <summary>
        ///  Verifica na configuração se vai usar SQLite
        /// </summary>
        /// <returns></returns>
        public static bool UsarSqlite()
        {
            string usarLite = ConfigurationManager.AppSettings.Get("UsarSqlite");

            if (!string.IsNullOrWhiteSpace(usarLite) && usarLite.ToLower().Equals("true"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Sqlite, SqlServer, Firebird, Oracle
        /// </summary>
        /// <returns></returns>
        public static TipoBanco TipoBancoDados()
        {
            string tipoBancoDados = ConfigurationManager.AppSettings.Get("TipoBanco");
            if (string.Compare(tipoBancoDados, "Sqlite", true) == 0)
            {
                return TipoBanco.Sqlite;
            }
            if (string.Compare(tipoBancoDados, "SqlServer", true) == 0)
            {
                return TipoBanco.SqlServer;
            }
            if (string.Compare(tipoBancoDados, "Firebird", true) == 0)
            {
                return TipoBanco.Firebird;
            }
            if (string.Compare(tipoBancoDados, "Oracle", true) == 0)
            {
                return TipoBanco.Oracle;
            }
            return TipoBanco.SqlServer;
        }

        /// <summary>
        /// Retorna o nome da string de conexão para Sqlite ou SqlServer
        /// </summary>
        /// <returns></returns>
        public static string ObterConexao()
        {
            if (TipoBancoDados() == TipoBanco.Sqlite)
            {
                return "name=ConcreteApiContextoSqlite";
            }
            if (TipoBancoDados() == TipoBanco.SqlServer)
            {
                return "name=ConcreteApiContextoSqlServer";
            }
            if (TipoBancoDados() == TipoBanco.Firebird)
            {
                return "name=ConcreteApiContextoFirebird";
            }
            if (TipoBancoDados() == TipoBanco.Oracle)
            {
                return "name=ConcreteApiContextoOracle";
            }
            return "name=ConcreteApiContextoSqlServer";
        }

        /// <summary>
        /// Inicializa o DataDirectory no AppDomain para aplicação que não tem definido por padrão
        /// </summary>
        public static void InicializaDataDiretory()
        {
            string caminho = Convert.ToString(AppDomain.CurrentDomain.GetData("DataDirectory"));

            if (string.IsNullOrWhiteSpace(caminho))
            {
                string caminhoRelativo = AppDomain.CurrentDomain.BaseDirectory;
                string caminhoCompleto = string.Empty;
                int pos = caminhoRelativo.IndexOf("x86") > 0 ? caminhoRelativo.IndexOf("bin\\x86") : caminhoRelativo.IndexOf("bin\\x64");
                if (pos > 0)
                {
                    caminhoCompleto = System.IO.Path.Combine(caminhoRelativo.Substring(0, pos), "App_Data");
                }
                else
                {
                    caminhoCompleto = System.IO.Path.Combine(caminhoRelativo, "..\\..\\App_Data");
                }
                if (!Directory.Exists(caminhoCompleto))
                {
                    Directory.CreateDirectory(caminhoCompleto);
                }
                // Inicializa o DataDirectory para os arquivos de dados
                AppDomain.CurrentDomain.SetData("DataDirectory", caminhoCompleto);
            }
        }

    }
}
