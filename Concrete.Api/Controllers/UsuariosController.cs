﻿using Concrete.Api.Cabecalho;
using Concrete.Modelo.Entidades;
using Concrete.Modelo.Repositorio;
using Concrete.Modelo.ViewModels;
using Concrete.Util;
using JWT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Concrete.Api.Controllers
{
    public class UsuariosController : ApiController
    {
        private IUsuarioRepositorio repo;

        public UsuariosController(IUsuarioRepositorio Irepo)
        {
            repo = Irepo;
        }

        // GET: api/SignUp
        public IQueryable<Usuario> GetUsuarios()
        {
            return repo.obterTodos().AsQueryable<Usuario>();
        }

        // GET: api/SignUp/5
        [ResponseType(typeof(Usuario))]
        public IHttpActionResult GetUsuario(int id)
        {
            Usuario usuario = repo.ObterPorId(id);
            if (usuario == null)
            {
                var respErro = new ErroViewModel(Convert.ToInt32(HttpStatusCode.NotFound), "Usuário não encontrado");
                HttpResponseMessage response = Request.CreateResponse<ErroViewModel>(HttpStatusCode.NotFound, respErro);
                return ResponseMessage(response);
            }

            return Ok(usuario);
        }

        /// <summary>
        /// Este endpoint deverá receber um usuário com os seguintes campos [Nome, email, senha, lista(telefone)]
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("SignUp")]
        [ResponseType(typeof(Usuario))]
        public IHttpActionResult SignUp(SignUpViewModel model)
        {
            HttpResponseMessage response;

            if (ModelState.IsValid)
            {
                var usuarioExistente = repo.obterPorEmail(model.Email);

                if (usuarioExistente != null)
                {
                    var respErro = new ErroViewModel(Convert.ToInt32(HttpStatusCode.BadRequest), "Usuário já existe");
                    response = Request.CreateResponse<ErroViewModel>(HttpStatusCode.BadRequest, respErro);
                    return ResponseMessage(response);
                }

                //Cria o novo usuário e salva no banco de dados
                var usuario = CriaUsuario(model);

                UsuarioHeader dbUsuario;

                //Cria o token
                var token = CriaToken(usuario, out dbUsuario);

                //Criptogafia não reversível (hash) no token
                string TokenSaltGerado = Criptografia.GerarSalt();
                string TokenHashGerado = Criptografia.Criptografa(token, TokenSaltGerado);

                usuario.DataCriacao = DateTime.Now;
                usuario.DataAtualizacao = DateTime.Now;
                usuario.UltimoLogin = DateTime.Now;

                //Grava no registro do usuário o salt e o token criptografado
                usuario.TokenSalt = TokenSaltGerado;
                usuario.Token = TokenHashGerado;

                //atualiza os dados após inclusão do novo usuário
                repo.atualizar(usuario);

                // Atualiza o token gerado para refletir a criptografia inreversível
                dbUsuario.XToken = TokenSaltGerado;

                response = Request.CreateResponse(new { dbUsuario, token });
                response.Headers.Add("X-Login-Id", dbUsuario.XId.ToString());
                response.Headers.Add("X-Login-Token", dbUsuario.XToken);

            }
            else
            {
                var respErro = new ErroViewModel(Convert.ToInt32(HttpStatusCode.BadRequest), "Não foi possível cadastrar o usuário");
                response = Request.CreateResponse<ErroViewModel>(HttpStatusCode.BadRequest, respErro);
            }

            return ResponseMessage(response);

        }

        [AllowAnonymous]
        [Route("Login")]
        [ResponseType(typeof(Usuario))]
        public IHttpActionResult Login(LoginViewModel model)
        {
            HttpResponseMessage response = null;
            if (ModelState.IsValid)
            {
                var usuarioExistente = repo.obterPorEmail(model.Email);

                if (usuarioExistente == null)
                {
                    // Usuário não encontrado por email
                    var respErro = new ErroViewModel(Convert.ToInt32(HttpStatusCode.NotFound), "Usuário não encontrado");
                    response = Request.CreateResponse<ErroViewModel>(HttpStatusCode.NotFound, respErro);
                }
                else
                {
                    var loginSuccess = string.Equals(Criptografia.Criptografa(model.Senha, usuarioExistente.SenhaSalt), usuarioExistente.Senha);

                    if (loginSuccess)
                    {
                        // Usuário Autenticado corretamente

                        UsuarioHeader dbUsuario;

                        //Cria o token
                        var token = CriaToken(usuarioExistente, out dbUsuario);

                        //Criptogafia não reversível (hash) no token
                        string TokenSaltGerado = Criptografia.GerarSalt();
                        string TokenHashGerado = Criptografia.Criptografa(token, TokenSaltGerado);

                        // Atualiza: Data do último login, salt e token
                        usuarioExistente.UltimoLogin = DateTime.Now;

                        //Grava no registro do usuário o salt e o token criptografado
                        usuarioExistente.TokenSalt = TokenSaltGerado;
                        usuarioExistente.Token = TokenHashGerado;

                        // Atualiza dados do usuário
                        repo.atualizar(usuarioExistente);

                        // Atualiza o token gerado para refletir a criptografia inreversível
                        dbUsuario.XToken = TokenSaltGerado;

                        response = Request.CreateResponse(new { dbUsuario, token });
                        response.Headers.Add("X-Login-Id", dbUsuario.XId.ToString());
                        response.Headers.Add("X-Login-Token", dbUsuario.XToken);

                    }
                    else
                    {
                        // Usuário e senha inválidos
                        var respErro = new ErroViewModel(Convert.ToInt32(HttpStatusCode.BadRequest), "Email ou senha inválidos");
                        response = Request.CreateResponse<ErroViewModel>(HttpStatusCode.BadRequest, respErro);
                    }
                }
            }
            else
            {
                var respErro = new ErroViewModel(Convert.ToInt32(HttpStatusCode.BadRequest), "Dados inválidos");
                response = Request.CreateResponse<ErroViewModel>(HttpStatusCode.BadRequest, respErro);
            }
            return ResponseMessage(response);
        }

        /// <summary>
        /// Chamadas para este endpoint devem conter um header na requisição de Authentication com o valor "Bearer {token}" onde { token}
        /// é o valor do recebido através do SignUp ou Login de um usuário.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="headers"></param>
        /// <returns></returns>
        [Authorize]
        [Route("Profile")]
        [ResponseType(typeof(Usuario))]
        public IHttpActionResult Profile(int id, [FromHeader]UsuarioHeader headers)
        {
            HttpResponseMessage response = null;

            //- Caso o token não exista, retornar erro com status apropriado com a mensagem "Não autorizado".
            if (headers == null || string.IsNullOrWhiteSpace(headers.XToken))
            {
                var respErro = new ErroViewModel(Convert.ToInt32(HttpStatusCode.Forbidden), "Não autorizado");
                response = Request.CreateResponse<ErroViewModel>(HttpStatusCode.Forbidden, respErro);
                return ResponseMessage(response);
            }

            //- Caso o token exista, buscar o usuário pelo id passado através da query string e comparar se o token do usuário encontrado é igual ao token passado no header.
            Usuario usuario = repo.ObterPorId(id);

            if (usuario == null)
            {
                var respErro = new ErroViewModel(Convert.ToInt32(HttpStatusCode.NotFound), "Usuário não encontrado");
                response = Request.CreateResponse<ErroViewModel>(HttpStatusCode.NotFound, respErro);
                return ResponseMessage(response);
            }

            //-Caso não seja o mesmo token, retornar erro com status apropriado e mensagem "Não autorizado"
            if (string.Compare(usuario.Token, headers.XToken, false) != 0)
            {
                var respErro = new ErroViewModel(Convert.ToInt32(HttpStatusCode.Forbidden), "Não autorizado");
                response = Request.CreateResponse<ErroViewModel>(HttpStatusCode.Forbidden, respErro);
                return ResponseMessage(response);
            }

            //- Caso seja o mesmo token, verificar se o último login foi a MENOS que 30 minutos atrás.
            //- Caso não seja a MENOS que 30 minutos atrás, retornar erro com status apropriado com mensagem "Sessão inválida
            if (DateTime.Compare(usuario.UltimoLogin.Value.AddMinutes(30), DateTime.Now) < 0)
            {
                var respErro = new ErroViewModel(Convert.ToInt32(HttpStatusCode.Forbidden), "Sessão inválida");
                response = Request.CreateResponse<ErroViewModel>(HttpStatusCode.Forbidden, respErro);
                return ResponseMessage(response);
            }

            //- Caso tudo esteja ok, retornar os dados do usuário.
            return Ok(usuario);
        }

        /// <summary>
        /// Create a Jwt with user information
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="dbUsuario"></param>
        /// <returns></returns>
        private static string CriaToken(Usuario usuario, out UsuarioHeader dbUsuario)
        {
            var unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var expiry = Math.Round((DateTime.UtcNow.AddHours(2) - unixEpoch).TotalSeconds);
            var issuedAt = Math.Round((DateTime.UtcNow - unixEpoch).TotalSeconds);
            var notBefore = Math.Round((DateTime.UtcNow.AddMonths(6) - unixEpoch).TotalSeconds);

            var payload = new Dictionary<string, object>
            {
                {"email", usuario.Email},
                {"usuarioId", usuario.UsuarioId},
                {"role", "Admin"},
                {"sub", usuario.UsuarioId},
                {"nbf", notBefore},
                {"iat", issuedAt},
                {"exp", expiry}
            };

            const string apikey = "chavesecreta";

            var token = JsonWebToken.Encode(payload, apikey, JwtHashAlgorithm.HS256);

            dbUsuario = new UsuarioHeader { XId = usuario.UsuarioId, XToken = token };
            return token;
        }

        /// <summary>
        /// Create a new user and saves it to the database
        /// </summary>
        /// <param name="registerDetails"></param>
        /// <returns></returns>
        private Usuario CriaUsuario(SignUpViewModel registerDetails)
        {
            //Criptogafia não reversível (hash) na senha
            var passwordSalt = Criptografia.GerarSalt();
            var password = Criptografia.Criptografa(registerDetails.Senha, passwordSalt);

            var user = new Usuario
            {
                Nome = registerDetails.Nome,
                Email = registerDetails.Email,
                SenhaSalt = passwordSalt,
                Senha = password,
                Telefones = registerDetails.Telefones
            };

            repo.incluir(user);

            return user;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                repo.Dispose();
            }
            base.Dispose(disposing);
        }

    }

    public class UsuarioHeader
    {
        public int XId { get; set; }
        public string XToken { get; set; }
    }

}