﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using System.Net.Http.Formatting;
using System.Web.Http.Dispatcher;
using System.Web.Http.Controllers;

namespace Concrete.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //// JWT como token generator
            //config.MessageHandlers.Add(new AuthHandler());

            //Todos os endpoints devem trabalhar somente com JSONs
            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());

            var json = config.Formatters.JsonFormatter;

            //To write indented JSON, set the Formatting setting to Formatting.Indented
            json.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;

            // Handling Circular Object References
            //json.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.All;

        }
    }
}
