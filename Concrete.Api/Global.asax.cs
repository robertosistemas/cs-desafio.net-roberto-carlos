﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using Concrete.Api.Controllers;
using Concrete.Modelo.Repositorio;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Concrete.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Injeção de dependência
            RegisterInjecaoDependencia();

        }

        private void RegisterInjecaoDependencia()
        {
            var builder = new ContainerBuilder();

            // Registra controllers MVC
            builder.RegisterControllers(typeof(WebApiApplication).Assembly);
            
            // Registra controllers WEB API
            builder.RegisterApiControllers(typeof(WebApiApplication).Assembly);

            // Registra os componentes
            builder.RegisterType<TelefoneRepositorioEF>().As<ITelefoneRepositorio>().InstancePerRequest();
            builder.RegisterType<UsuarioRepositorioEF>().As<IUsuarioRepositorio>().InstancePerRequest();

            AutofacBootstrap.Init(builder);

            // Define resolvedor de dependencia para ser Autofac
            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);

        }

    }

    public class AutofacBootstrap
    {
        internal static void Init(ContainerBuilder builder)
        {
        }
    }

}