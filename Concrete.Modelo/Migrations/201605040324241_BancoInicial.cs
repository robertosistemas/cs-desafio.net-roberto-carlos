namespace Concrete.Modelo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BancoInicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "HR.Telefones",
                c => new
                    {
                        TelefoneId = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Numero = c.String(maxLength: 20),
                        Ddd = c.String(maxLength: 2),
                        UsuarioId = c.Decimal(nullable: false, precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.TelefoneId)
                .ForeignKey("HR.Usuarios", t => t.UsuarioId, cascadeDelete: true)
                .Index(t => t.UsuarioId);
            
            CreateTable(
                "HR.Usuarios",
                c => new
                    {
                        UsuarioId = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Nome = c.String(maxLength: 100),
                        Email = c.String(maxLength: 100),
                        SenhaSalt = c.String(maxLength: 100),
                        Senha = c.String(maxLength: 100),
                        DataCriacao = c.DateTime(),
                        DataAtualizacao = c.DateTime(),
                        UltimoLogin = c.DateTime(),
                        TokenSalt = c.String(maxLength: 100),
                        Token = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.UsuarioId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("HR.Telefones", "UsuarioId", "HR.Usuarios");
            DropIndex("HR.Telefones", new[] { "UsuarioId" });
            DropTable("HR.Usuarios");
            DropTable("HR.Telefones");
        }
    }
}
