﻿using System.Data.Entity;

namespace Concrete.Modelo.Base
{
    public static class DbContextUtils<TContext>
        where TContext : DbContext
    {
        static object _InitializeLock = new object();
        static bool _InitializeLoaded = false;

        /// <summary>
        /// Method to allow running a DatabaseInitializer exactly once
        /// </summary>   
        /// <param name="initializer">A Database Initializer to run</param>
        public static void SetInitializer(IDatabaseInitializer<TContext> initializer = null)

        {
            if (_InitializeLoaded)
                return;

            // watch race condition
            lock (_InitializeLock)
            {
                // are we sure?
                if (_InitializeLoaded)
                    return;

                _InitializeLoaded = true;

                // force Initializer to load only once
               Database.SetInitializer<TContext>(initializer);
            }
        }
    }
}
