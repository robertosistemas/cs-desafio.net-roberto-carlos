﻿using System.Data.Entity;
using Concrete.Modelo.Entidades;
using Concrete.Modelo.Base;
using System;
using System.Linq;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.IO;
using System.Configuration;
using Concrete.Util;

namespace Concrete.Modelo.Base
{
    public class ConcreteApiContexto : DbContext
    {

        static ConcreteApiContexto()
        {
            Configuracoes.InicializaDataDiretory();
        }

        public ConcreteApiContexto() : base(Configuracoes.ObterConexao())
        {
            // Para serializar as entidades corretamento no serviço WCF
            this.Configuration.ProxyCreationEnabled = false;
            //this.Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Telefone> Telefones { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Não será pluralizado o nome de tabelas
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.HasDefaultSchema("HR");

            //if (Configuracoes.TipoBancoDados() == TipoBanco.Sqlite)
            //{
            //    DbContextUtils<ConcreteApiContexto>.SetInitializer(new SqliteDbInitializer(modelBuilder));
            //}
            //if (Configuracoes.TipoBancoDados() == TipoBanco.SqlServer)
            //{
            //    DbContextUtils<ConcreteApiContexto>.SetInitializer(new SqlServerDbInitializer(modelBuilder));
            //}
            //if (Configuracoes.TipoBancoDados() == TipoBanco.Firebird)
            //{
            //    //TODO: Criar contexto para Firebird
            //    DbContextUtils<ConcreteApiContexto>.SetInitializer(new SqlServerDbInitializer(modelBuilder));
            //}
            //if (Configuracoes.TipoBancoDados() == TipoBanco.Oracle)
            //{
            //    //TODO: Criar contexto para Oracle
            //    DbContextUtils<ConcreteApiContexto>.SetInitializer(new SqlServerDbInitializer(modelBuilder));
            //}

            //modelBuilder.Entity<Telefone>()
            //    .HasOptional<Usuario>(s => s.Usuario)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            base.OnModelCreating(modelBuilder);
        }

        public static ConcreteApiContexto Criar()
        {
            return new ConcreteApiContexto();
        }

        public override int SaveChanges()
        {
            DateTime saveTime = DateTime.Now;
            foreach (var entry in this.ChangeTracker.Entries<Usuario>().Where(e => e.State == (EntityState)EntityState.Added || e.State == (EntityState)EntityState.Modified))
            {
                if (entry.Property("DataCriacao").CurrentValue == null)
                {
                    entry.Property("DataCriacao").CurrentValue = saveTime;
                }
                entry.Property("DataAtualizacao").CurrentValue = saveTime;
            }
            return base.SaveChanges();
        }

    }
}
