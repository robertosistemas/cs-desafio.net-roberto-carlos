﻿using System.Data.Entity;
using System.Reflection;
using SQLite.CodeFirst;
using Concrete.Util;
using System;
using Concrete.Modelo.Entidades;
using System.Data.Entity.Migrations;

namespace Concrete.Modelo.Base
{
    public class SqliteDbInitializer : SqliteCreateDatabaseIfNotExists<ConcreteApiContexto>
    {

        public SqliteDbInitializer(DbModelBuilder modelBuilder) : base(modelBuilder)
        {
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetAssembly(typeof(Migrations.Configuration)));
        }
        public SqliteDbInitializer(DbModelBuilder modelBuilder, bool nullByteFileMeansNotExisting) : base(modelBuilder, nullByteFileMeansNotExisting)
        {
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetAssembly(typeof(Migrations.Configuration)));
        }

        protected override void Seed(ConcreteApiContexto context)
        {
            string SenhaSaltGerado = Criptografia.GerarSalt();
            string SenhaHashGerado = Criptografia.Criptografa("1234", SenhaSaltGerado);

            string TokenSaltGerado = Criptografia.GerarSalt();
            string TokenHashGerado = Criptografia.Criptografa(Guid.NewGuid().ToString(), TokenSaltGerado);

            Usuario usuario = new Usuario { UsuarioId = 1, Nome = "Roberto Carlos", Email = "robertosistemas@hotmail.com", SenhaSalt = SenhaSaltGerado, Senha = SenhaHashGerado, TokenSalt = TokenSaltGerado, Token = TokenHashGerado };

            context.Usuarios.AddOrUpdate<Usuario>(usuario);

            SenhaSaltGerado = Criptografia.GerarSalt();
            SenhaHashGerado = Criptografia.Criptografa("4321", SenhaSaltGerado);

            TokenSaltGerado = Criptografia.GerarSalt();
            TokenHashGerado = Criptografia.Criptografa(Guid.NewGuid().ToString(), TokenSaltGerado);

            context.Telefones.AddOrUpdate<Telefone>(new Telefone { TelefoneId = 1, Ddd = "21", Numero = "999619795", UsuarioId = usuario.UsuarioId });
            context.Telefones.AddOrUpdate<Telefone>(new Telefone { TelefoneId = 2, Ddd = "21", Numero = "999617572", UsuarioId = usuario.UsuarioId });

            usuario = new Usuario { UsuarioId = 2, Nome = "Fulano da Silva Junior", Email = "teste@teste.com", SenhaSalt = SenhaSaltGerado, Senha = SenhaHashGerado, TokenSalt = TokenSaltGerado, Token = TokenHashGerado };

            context.Usuarios.AddOrUpdate<Usuario>(usuario);

            context.Telefones.AddOrUpdate<Telefone>(new Telefone { TelefoneId = 3, Ddd = "21", Numero = "925363644", UsuarioId = usuario.UsuarioId });
            context.Telefones.AddOrUpdate<Telefone>(new Telefone { TelefoneId = 4, Ddd = "21", Numero = "977733344", UsuarioId = usuario.UsuarioId });

            base.Seed(context);
        }

    }
}