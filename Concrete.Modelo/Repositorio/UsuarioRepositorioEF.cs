﻿using Concrete.Modelo.Base;
using Concrete.Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Concrete.Modelo.Repositorio
{
    public class UsuarioRepositorioEF : IUsuarioRepositorio
    {

        private bool contextoExterno = false;
        private ConcreteApiContexto db;

        public UsuarioRepositorioEF()
        {
            db = new ConcreteApiContexto();
        }

        public UsuarioRepositorioEF(ConcreteApiContexto contexto)
        {
            contextoExterno = true;
            db = contexto;
        }

        public ConcreteApiContexto Contexto
        {
            get
            {
                return db;
            }
            set
            {
                contextoExterno = true;
                db = value;
            }
        }

        public void incluir(Usuario entidade)
        {
            db.Usuarios.Add(entidade);
            db.SaveChanges();
            //foreach (Telefone telefone in entidade.Telefones)
            //{
            //    telefone.UsuarioId = entidade.UsuarioId;
            //    db.Telefones.Add(telefone);
            //}
            //db.SaveChanges();
        }

        public void atualizar(Usuario entidade)
        {
            Usuario usuario = db.Usuarios.Find(entidade.UsuarioId);
            if (usuario != null)
            {
                //List<Telefone> lista = TelefoneRepositorioEF.CopiaLista(entidade.Telefones.ToList<Telefone>());
                //db.Telefones.RemoveRange(db.Telefones.Where(p => p.UsuarioId == entidade.UsuarioId));
                //foreach (Telefone telefone in lista)
                //{
                //    telefone.UsuarioId = entidade.UsuarioId;
                //    db.Telefones.Add(telefone);
                //}
                db.Entry(entidade).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void excluir(int id)
        {
            Usuario usuario = db.Usuarios.Find(id);
            if (usuario != null)
            {
                //db.Telefones.RemoveRange(db.Telefones.Where(p => p.UsuarioId == usuario.UsuarioId));
                db.Usuarios.Remove(usuario);
                db.SaveChanges();
            }
        }

        public List<Usuario> obterTodos()
        {
            return db.Usuarios
                 .Include(e => e.Telefones)
                 .ToList<Usuario>();
        }

        public Usuario ObterPorId(int id)
        {
            Usuario usuario = db.Usuarios.Find(id);
            if (usuario != null)
            {
                usuario.Telefones = db.Telefones.Where(e => e.UsuarioId == usuario.UsuarioId).ToList<Telefone>();
            }
            return usuario;
        }

        public Usuario obterPorEmail(string email)
        {
            Usuario usuario = db.Usuarios.FirstOrDefault<Usuario>(item => item.Email == email);
            if (usuario != null)
            {
                usuario.Telefones = db.Telefones.Where(e => e.UsuarioId == usuario.UsuarioId).ToList<Telefone>();
            }
            return usuario;
        }

        public bool Existe(int id)
        {
            return db.Usuarios.Count(e => e.UsuarioId == id) > 0;
        }

        public void ApagarTudo()
        {
            //db.Telefones.RemoveRange(db.Telefones.ToList<Telefone>());
            //db.SaveChanges();
            db.Usuarios.RemoveRange(db.Usuarios.ToList<Usuario>());
            db.SaveChanges();
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (!contextoExterno && db != null)
                    {
                        db.Dispose();
                    }
                }
                disposedValue = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);
        }
        #endregion

    }
}
