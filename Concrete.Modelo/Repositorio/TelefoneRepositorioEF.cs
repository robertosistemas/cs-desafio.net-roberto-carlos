﻿using Concrete.Modelo.Base;
using Concrete.Modelo.Entidades;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Concrete.Modelo.Repositorio
{
    public class TelefoneRepositorioEF : ITelefoneRepositorio
    {

        private bool contextoExterno = false;
        private ConcreteApiContexto db;

        public TelefoneRepositorioEF()
        {
            db = new ConcreteApiContexto();
        }

        public TelefoneRepositorioEF(ConcreteApiContexto contexto)
        {
            contextoExterno = true;
            db = contexto;
        }

        public ConcreteApiContexto Contexto
        {
            get
            {
                return db;
            }
            set
            {
                contextoExterno = true;
                db = value;
            }
        }

        public void incluir(Telefone entidade)
        {
            db.Telefones.Add(entidade);
            db.SaveChanges();
        }

        public void atualizar(Telefone entidade)
        {
            db.Entry(entidade).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void excluir(int id)
        {
            Telefone telefone = db.Telefones.Find(id);
            db.Telefones.Remove(telefone);
            db.SaveChanges();
        }

        public List<Telefone> obterTodos()
        {
            return db.Telefones.ToList<Telefone>();
        }

        public Telefone ObterPorId(int id)
        {
            Telefone telefone = db.Telefones.Find(id);
            return telefone;
        }

        public bool Existe(int id)
        {
            return db.Telefones.Count(e => e.TelefoneId == id) > 0;
        }

        public void ApagarTudo()
        {
            db.Telefones.RemoveRange(db.Telefones.ToList<Telefone>());
            db.SaveChanges();
        }

        public Telefone ObterDddNumero(string Ddd, string Numero)
        {
            Telefone telefone = db.Telefones.FirstOrDefault<Telefone>(item => item.Ddd == Ddd && item.Numero == Numero);
            return telefone;
        }

        public List<Telefone> ObterPorUsuarioId(int usuarioId)
        {
            return db.Telefones.Where(p => p.UsuarioId == usuarioId).ToList<Telefone>();
        }

        public void ExcluirPorUsuarioId(int usuarioId)
        {
            db.Telefones.RemoveRange(db.Telefones.Where(p => p.UsuarioId == usuarioId));
            db.SaveChanges();
        }

        public static List<Telefone> CopiaLista(List<Telefone> lista)
        {
            List<Telefone> retorno = new List<Telefone>();
            foreach (Telefone item in lista)
            {
                retorno.Add(new Telefone { TelefoneId = item.TelefoneId, Numero = item.Numero, Ddd = item.Ddd, UsuarioId = item.UsuarioId, Usuario = item.Usuario });
            }
            return retorno;
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (!contextoExterno && db != null)
                    {
                        db.Dispose();
                    }
                }
                disposedValue = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);
        }
        #endregion

    }
}
