﻿using System;
using Concrete.Modelo.Entidades;

namespace Concrete.Modelo.Repositorio
{
    public interface IUsuarioRepositorio : IRepositorio<Usuario>
    {
        Usuario obterPorEmail(string email);
    }
}
