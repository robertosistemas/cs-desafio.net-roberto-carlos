﻿using System;
using System.Collections.Generic;

namespace Concrete.Modelo.Repositorio
{
    public interface IRepositorio<T> : IDisposable
    {
        void incluir(T entidade);
        void atualizar(T entidade);
        void excluir(int id);
        T ObterPorId(int id);
        List<T> obterTodos();
        bool Existe(int id);
        void ApagarTudo();
    }
}
