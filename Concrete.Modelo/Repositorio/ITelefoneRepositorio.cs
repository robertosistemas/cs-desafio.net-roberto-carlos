﻿using Concrete.Modelo.Entidades;
using System.Collections.Generic;

namespace Concrete.Modelo.Repositorio
{
    public interface ITelefoneRepositorio : IRepositorio<Telefone>
    {
        Telefone ObterDddNumero(string Ddd, string Numero);
        List<Telefone> ObterPorUsuarioId(int usuarioId);
        void ExcluirPorUsuarioId(int usuarioId);
    }
}
