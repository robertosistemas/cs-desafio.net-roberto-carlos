﻿using Concrete.Modelo.Entidades;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

/// <summary>
///  { "nome": "sample string", "email": "sample string", "senha": "sample string", "telefones": [ { "numero": "sample string", "Ddd": "sample string" } ] }
/// </summary>
namespace Concrete.Modelo.ViewModels
{
    /// <summary>
    /// SignUp - Criação de Cadastro
    /// </summary>
    [NotMapped]
    public class SignUpViewModel
    {
        public SignUpViewModel()
        {
            Telefones = new List<Telefone>();
        }

        [JsonProperty("nome")]
        [StringLength(100)]
        public string Nome { get; set; }

        [JsonProperty("email")]
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        [StringLength(100)]
        public string Email { get; set; }

        [JsonProperty("senha")]
        [Required]
        [Display(Name = "Senha")]
        [DataType(DataType.Password)]
        [StringLength(100)]
        public string Senha { get; set; }

        [JsonProperty("telefones")]
        public virtual ICollection<Telefone> Telefones { get; set; }

    }
}
