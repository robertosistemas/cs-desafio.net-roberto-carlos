﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

/// <summary>
///  {"email": "sample string",	"senha": "sample string"}
/// </summary>
namespace Concrete.Modelo.ViewModels
{
    /// <summary>
    /// Login - Autenticação de usuário
    /// </summary>
    [NotMapped]
    public class LoginViewModel
    {
        [JsonProperty("email")]
        [Display(Name = "Email")]
        [Required]
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        [StringLength(100)]
        public string Email { get; set; }

        [JsonProperty("senha")]
        [Display(Name = "Senha")]
        [Required]
        [DataType(DataType.Password)]
        [StringLength(100)]
        public string Senha { get; set; }

        //[JsonProperty("lembrar")]
        //[Display(Name = "Manter Conectado")]
        //public bool Lembrar { get; set; }
    }
}
