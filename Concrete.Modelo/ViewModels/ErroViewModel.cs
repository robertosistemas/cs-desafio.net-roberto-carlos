﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;

/// <summary>
///  { "statusCode: 0, (StatusCode adequado) "mensagem": "sample string" (Mensagem adequada) }
/// </summary>
///
namespace Concrete.Modelo.ViewModels
{

    /// <summary>
    /// respostas de erro
    /// </summary>
    [NotMapped]
    public class ErroViewModel
    {

        public ErroViewModel(int statuCode, string mensagem)
        {
            this.StatusCode = statuCode;
            this.Mensagem = mensagem;
        }

        [JsonProperty("statusCode")]
        public int StatusCode { get; set; }

        [JsonProperty("mensagem")]
        public string Mensagem { get; set; }
    }
}
