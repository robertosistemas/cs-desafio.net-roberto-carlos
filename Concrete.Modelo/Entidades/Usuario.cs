﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Concrete.Modelo.Entidades
{

    //[DataContract(IsReference = true)]
    [Table("Usuarios", Schema = "HR")]
    public class Usuario
    {

        public Usuario()
        {
            Telefones = new List<Telefone>();
        }

        //[DatabaseGenerated(DatabaseGeneratedOption.Identity), Key]

        [JsonProperty("id")]
        [Key]
        public int UsuarioId { get; set; }

        [JsonProperty("nome")]
        [StringLength(100)]
        public string Nome { get; set; }

        [JsonProperty("email")]
        [StringLength(100)]
        public string Email { get; set; }

        [IgnoreDataMember, JsonIgnore, XmlIgnore]
        [StringLength(100)]
        public string SenhaSalt { get; set; }

        [JsonProperty("senha")]
        [StringLength(100)]
        public string Senha { get; set; }

        /// <summary>
        /// data da criação do usuário
        /// </summary>
        [JsonProperty("data_criacao")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? DataCriacao { get; set; }

        /// <summary>
        /// data da última atualização do usuário
        /// </summary>
        [JsonProperty("data_atualizacao")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? DataAtualizacao { get; set; }

        /// <summary>
        /// data do último login(no caso da criação, será a mesma que a criação
        /// </summary>
        [JsonProperty("ultimo_login")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? UltimoLogin { get; set; }

        /// <summary>
        /// Salt para criptografar o token
        /// </summary>
        [IgnoreDataMember, JsonIgnore, XmlIgnore]
        [StringLength(100)]
        public string TokenSalt { get; set; }

        /// <summary>
        /// token de acesso da API(pode ser um GUID ou um JWT)
        /// </summary>
        [JsonProperty("token")]
        [StringLength(100)]
        public string Token { get; set; }

        [JsonProperty("telefones")]
        public virtual ICollection<Telefone> Telefones { get; set; }

    }
}
