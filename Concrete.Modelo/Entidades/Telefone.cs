﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Concrete.Modelo.Entidades
{
    /// <summary>
    /// </summary>
    //[DataContract(IsReference = true)]
    [Table("Telefones", Schema = "HR")]
    public class Telefone
    {
        [IgnoreDataMember, JsonIgnore, XmlIgnore]
        [Key]
        public int TelefoneId { get; set; }

        [StringLength(20)]
        [JsonProperty("numero")]
        public string Numero { get; set; }

        [StringLength(2)]
        [JsonProperty("Ddd")]
        public string Ddd { get; set; }

        [IgnoreDataMember, JsonIgnore, XmlIgnore]
        [ForeignKey("Usuario")]
        public int UsuarioId { get; set; }

        [IgnoreDataMember, JsonIgnore, XmlIgnore]
        public virtual Usuario Usuario { get; set; }

    }
}
