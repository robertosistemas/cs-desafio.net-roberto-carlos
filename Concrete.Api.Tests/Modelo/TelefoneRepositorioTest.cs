﻿using Concrete.Modelo.Entidades;
using Concrete.Modelo.Repositorio;
using Concrete.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Concrete.Api.Tests.Modelo
{
    [TestClass]
    public class TelefoneRepositorioTest
    {

        private static UsuarioRepositorioEF repoUsu = null;
        private static TelefoneRepositorioEF repoTele = null;

        [ClassInitialize]
        public static void InicializaClass(TestContext contexto)
        {
            repoUsu = new UsuarioRepositorioEF();
            repoTele = new TelefoneRepositorioEF();
        }

        [ClassCleanup]
        public static void FinalizaClass()
        {
            repoTele.Dispose();
            repoUsu.Dispose();
        }

        [TestMethod]
        public void incluir()
        {
            string SenhaSaltGerado = Criptografia.GerarSalt();
            string SenhaHashGerado = Criptografia.Criptografa("segredos", SenhaSaltGerado);
            Usuario usuario = new Usuario { Nome = "Fulando da Silva", Email = "fulano@dominio.com.br", SenhaSalt = SenhaSaltGerado, Senha = SenhaHashGerado };
            repoUsu.incluir(usuario);

            Telefone telefone = new Telefone { Ddd = "21", Numero = "12345678", UsuarioId = usuario.UsuarioId };
            repoTele.incluir(telefone);
        }

        [TestMethod]
        public void atualizar()
        {
            string SenhaSaltGerado = Criptografia.GerarSalt();
            string SenhaHashGerado = Criptografia.Criptografa("segredos", SenhaSaltGerado);
            Usuario usuario = new Usuario { Nome = "Fulando da Silva", Email = "fulano@dominio.com.br", SenhaSalt = SenhaSaltGerado, Senha = SenhaHashGerado };
            repoUsu.incluir(usuario);

            Telefone telefone = new Telefone { Ddd = "21", Numero = "12345678", UsuarioId = usuario.UsuarioId };
            repoTele.incluir(telefone);

            telefone.Ddd = "35";
            telefone.Numero = "12121212";
            repoTele.atualizar(telefone);
        }

        [TestMethod]
        public void excluir()
        {
            string SenhaSaltGerado = Criptografia.GerarSalt();
            string SenhaHashGerado = Criptografia.Criptografa("segredos", SenhaSaltGerado);
            Usuario usuario = new Usuario { Nome = "Fulando da Silva", Email = "fulano@dominio.com.br", SenhaSalt = SenhaSaltGerado, Senha = SenhaHashGerado };
            repoUsu.incluir(usuario);

            Telefone telefone = new Telefone { Ddd = "21", Numero = "12345678", UsuarioId = usuario.UsuarioId };
            repoTele.incluir(telefone);

            repoTele.excluir(telefone.TelefoneId);

            repoUsu.excluir(usuario.UsuarioId);

        }

        [TestMethod]
        public void obterTodos()
        {
            List<Telefone> telefones = repoTele.obterTodos();
        }

        [TestMethod]
        public void ObterPorId()
        {
            Telefone telefone = repoTele.ObterPorId(1);
        }

        [TestMethod]
        public void Existe()
        {
            string SenhaSaltGerado = Criptografia.GerarSalt();
            string SenhaHashGerado = Criptografia.Criptografa("segredos", SenhaSaltGerado);
            Usuario usuario = new Usuario { Nome = "Fulando da Silva", Email = "fulano@dominio.com.br", SenhaSalt = SenhaSaltGerado, Senha = SenhaHashGerado };
            repoUsu.incluir(usuario);

            Telefone telefone = new Telefone { Ddd = "21", Numero = "12345678", UsuarioId = usuario.UsuarioId };
            repoTele.incluir(telefone);

            Assert.IsTrue(repoTele.Existe(telefone.TelefoneId));

        }

        //[TestMethod]
        //public void ApagarTudo()
        //{
        //    repoTele.ApagarTudo();
        //}

        [TestMethod]
        public void ObterDddNumero()
        {
            Telefone telefone = repoTele.ObterDddNumero("21", "12345678");
        }

        [TestMethod]
        public void ObterPorUsuarioId()
        {
            List<Telefone> telefones = repoTele.ObterPorUsuarioId(1);
        }

        [TestMethod]
        public void ExcluirPorUsuarioId()
        {
            repoTele.ExcluirPorUsuarioId(1);
        }
    }
}
