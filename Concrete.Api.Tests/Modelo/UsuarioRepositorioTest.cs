﻿using Concrete.Modelo.Entidades;
using Concrete.Modelo.Repositorio;
using Concrete.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Concrete.Api.Tests.Modelo
{
    [TestClass]
    public class UsuarioRepositorioTest
    {

        private static UsuarioRepositorioEF repo = null;

        [ClassInitialize]
        public static void InicializaClass(TestContext contexto)
        {
            repo = new UsuarioRepositorioEF();
        }

        [ClassCleanup]
        public static void FinalizaClass()
        {
            repo.Dispose();
        }

        [TestMethod]
        public void incluir()
        {
            string SenhaSaltGerado = Criptografia.GerarSalt();
            string SenhaHashGerado = Criptografia.Criptografa("segredos", SenhaSaltGerado);
            Usuario usuario = new Usuario { Nome = "Fulando da Silva", Email = "fulano@dominio.com.br", SenhaSalt = SenhaSaltGerado, Senha = SenhaHashGerado };
            usuario.Telefones.Add(new Telefone { Ddd = "21", Numero = "12345678" });
            usuario.Telefones.Add(new Telefone { Ddd = "21", Numero = "87654321" });
            repo.incluir(usuario);
        }

        [TestMethod]
        public void atualizar()
        {
            string SenhaSaltGerado = Criptografia.GerarSalt();
            string SenhaHashGerado = Criptografia.Criptografa("segredos", SenhaSaltGerado);
            Usuario usuario = new Usuario { Nome = "Fulando da Silva", Email = "fulano@dominio.com.br", SenhaSalt = SenhaSaltGerado, Senha = SenhaHashGerado };
            usuario.Telefones.Add(new Telefone { Ddd = "21", Numero = "12345678" });
            usuario.Telefones.Add(new Telefone { Ddd = "21", Numero = "87654321" });

            repo.incluir(usuario);

            usuario.Nome = "<Mudando de Nome>";
            repo.atualizar(usuario);
        }

        [TestMethod]
        public void excluir()
        {
            string SenhaSaltGerado = Criptografia.GerarSalt();
            string SenhaHashGerado = Criptografia.Criptografa("segredos", SenhaSaltGerado);
            Usuario usuario = new Usuario { Nome = "Fulando da Silva", Email = "fulano@dominio.com.br", SenhaSalt = SenhaSaltGerado, Senha = SenhaHashGerado };
            usuario.Telefones.Add(new Telefone { Ddd = "21", Numero = "12345678" });
            usuario.Telefones.Add(new Telefone { Ddd = "21", Numero = "87654321" });

            // Inclui para excluir
            repo.incluir(usuario);
            // Exclui
            repo.excluir(usuario.UsuarioId);

        }

        [TestMethod]
        public void obterTodos()
        {
            List<Usuario> usuarios = repo.obterTodos();
        }

        [TestMethod]
        public void ObterPorId()
        {
            Usuario usuario = repo.ObterPorId(1);
        }

        [TestMethod]
        public void obterPorEmail()
        {
            Usuario usuario = repo.obterPorEmail("fulano@dominio.com.br");
        }

        [TestMethod]
        public void Existe()
        {
            string SenhaSaltGerado = Criptografia.GerarSalt();
            string SenhaHashGerado = Criptografia.Criptografa("segredos", SenhaSaltGerado);
            Usuario usuario = new Usuario { Nome = "Fulando da Silva", Email = "fulano@dominio.com.br", SenhaSalt = SenhaSaltGerado, Senha = SenhaHashGerado };
            usuario.Telefones.Add(new Telefone { Ddd = "21", Numero = "12345678" });
            usuario.Telefones.Add(new Telefone { Ddd = "21", Numero = "87654321" });

            repo.incluir(usuario);

            Assert.IsTrue(repo.Existe(usuario.UsuarioId));
        }

        //[TestMethod]
        //public void ApagarTudo()
        //{
        //    repo.ApagarTudo();
        //}

    }
}
