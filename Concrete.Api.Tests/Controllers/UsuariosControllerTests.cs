﻿using Concrete.Modelo.Entidades;
using Concrete.Modelo.Repositorio;
using Concrete.Modelo.ViewModels;
using Concrete.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Concrete.Api.Controllers.Tests
{
    [TestClass()]
    public class UsuariosControllerTests
    {

        private static UsuarioRepositorioEF repoUsu = null;
        private static TelefoneRepositorioEF repoTele = null;

        [ClassInitialize]
        public static void InicializaClass(TestContext contexto)
        {
            repoUsu = new UsuarioRepositorioEF();
            repoTele = new TelefoneRepositorioEF();
        }

        [ClassCleanup]
        public static void FinalizaClass()
        {
            repoTele.Dispose();
            repoUsu.Dispose();
        }

        [TestMethod()]
        public void UsuariosControllerTest()
        {
            // Arrange
            var request = new HttpRequestMessage();
            var controller = new UsuariosController(repoUsu);
            var controllerContext = new HttpControllerContext();

            //controllerContext.Request = request;
            controller.ControllerContext = controllerContext;
            controller.Request = request;
            controller.Request.SetConfiguration(new HttpConfiguration());

            var resultado = controller.GetUsuarios();
            Assert.IsNotNull(resultado);
        }

        [TestMethod()]
        public void GetUsuariosTest()
        {
            // Arrange
            var request = new HttpRequestMessage();
            var controller = new UsuariosController(repoUsu);
            var controllerContext = new HttpControllerContext();

            //controllerContext.Request = request;
            controller.ControllerContext = controllerContext;
            controller.Request = request;
            controller.Request.SetConfiguration(new HttpConfiguration());

            var resultado = controller.GetUsuarios();
            Assert.IsNotNull(resultado);
        }

        [TestMethod()]
        public void GetUsuarioTest()
        {
            // Arrange
            var request = new HttpRequestMessage();
            var controller = new UsuariosController(repoUsu);
            var controllerContext = new HttpControllerContext();

            //controllerContext.Request = request;
            controller.ControllerContext = controllerContext;
            controller.Request = request;
            controller.Request.SetConfiguration(new HttpConfiguration());

            var resultado = controller.GetUsuario(1);
            Assert.IsNotNull(resultado);
        }

        [TestMethod()]
        public void SignUpTest()
        {
            // Arrange
            var controller = new UsuariosController(repoUsu);
            var controllerContext = new HttpControllerContext();
            var request = new HttpRequestMessage();

            //controllerContext.Request = request;
            controller.ControllerContext = controllerContext;
            controller.Request = request;
            controller.Request.SetConfiguration(new HttpConfiguration());

            SignUpViewModel novoUsuario = new SignUpViewModel
            {
                Nome = "Roberto Carlos novo",
                Email = "novoemail@email.com",
                Senha = "testando123"
            };

            novoUsuario.Telefones.Add(new Telefone { Ddd = "21", Numero = "91919292" });
            novoUsuario.Telefones.Add(new Telefone { Ddd = "35", Numero = "45457878" });

            IHttpActionResult resultado = controller.SignUp(novoUsuario);
            Assert.IsNotNull(resultado);

        }

        [TestMethod()]
        public void LoginTest()
        {
            // Arrange
            var request = new HttpRequestMessage();
            var controller = new UsuariosController(repoUsu);
            var controllerContext = new HttpControllerContext();

            //controllerContext.Request = request;
            controller.ControllerContext = controllerContext;
            controller.Request = request;
            controller.Request.SetConfiguration(new HttpConfiguration());

            LoginViewModel Usuario = new LoginViewModel
            {
                Email = "novoemail@email.com",
                Senha = "testando123"
            };

            IHttpActionResult resultado = controller.Login(Usuario);
            Assert.IsNotNull(resultado);
        }

        [TestMethod()]
        public void ProfileTest()
        {

            // Cria o usuário
            SignUpTest();
            
            // Faz login para atualizar último acesso e token
            LoginTest();

            // Arrange
            var request = new HttpRequestMessage();
            var controller = new UsuariosController(repoUsu);
            var controllerContext = new HttpControllerContext();

            //controllerContext.Request = request;
            controller.ControllerContext = controllerContext;
            controller.Request = request;
            controller.Request.SetConfiguration(new HttpConfiguration());

            // Procura o usuário por e-mail
            var usuario = repoUsu.obterPorEmail("novoemail@email.com");

            if (usuario != null)
            {

                var dbUsuario = new UsuarioHeader { XId = usuario.UsuarioId, XToken = usuario.Token };

                request.Headers.Add("X-Login-Id", dbUsuario.XId.ToString());
                request.Headers.Add("X-Login-Token", dbUsuario.XToken);

                // Act
                var resultado = controller.Profile(usuario.UsuarioId, dbUsuario);

                // Assert
                Assert.IsNotNull(resultado);

            }

        }
    }
}