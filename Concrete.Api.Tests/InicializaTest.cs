﻿using Concrete.Modelo.Base;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Concrete.Api.Tests
{
    [TestClass]
    public class InicializaTest
    {

        [AssemblyInitialize]
        public static void InicializaAssembly(TestContext contexto)
        {
            using (ConcreteApiContexto repoAssembly = new ConcreteApiContexto())
            {
                repoAssembly.Telefones.RemoveRange(repoAssembly.Telefones.ToList());
                repoAssembly.Usuarios.RemoveRange(repoAssembly.Usuarios.ToList());
                repoAssembly.SaveChanges();
            }
        }

    }
}